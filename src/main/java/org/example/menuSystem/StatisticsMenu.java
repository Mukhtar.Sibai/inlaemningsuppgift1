package org.example.menuSystem;

import java.util.List;
import org.example.staff.Employee;
import org.example.staff.Staff;

public class StatisticsMenu extends Menu {
  public StatisticsMenu() {
    super("Statistics Menu");
    menuOptions =
        List.of(
            new MenuOption(1, "Get Total Number Of Staff", Staff::getNumberOfStaff),
            new MenuOption(
                2, "Get The Average Salary For Men ", Employee::getTheAverageSalaryForMen),
            new MenuOption(
                3, "Get The Average Salary For Women ", Employee::getTheAverageSalaryForWomen),
            new MenuOption(4, "Get A List Of Employees Arranged By Date", Employee::arrangeByDate),
            new MenuOption(5, "Get A List Of All Staff", Staff::viewStaffList),
            new MenuOption(6, "Return To Main Menu", () -> MenuSystem.setState(new MainMenu())));
  }
}
