package org.example.menuSystem;

public class MenuOption {
  private final int optionNumber;
  private final String optionText;
  private final Runnable action;

  public MenuOption(int optionNumber, String optionText, Runnable action) {
    this.optionNumber = optionNumber;
    this.optionText = optionText;
    this.action = action;
  }

  public String getOptionText() {
    return optionText;
  }

  public int getOptionNumber() {
    return optionNumber;
  }

  public void run() {
    action.run();
  }
}
