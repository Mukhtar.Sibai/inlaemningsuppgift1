package org.example.menuSystem;

public class MenuSystem implements MenuState {
  private static MenuState activeMenu;

  public MenuSystem(MenuState activeMenu) {
    MenuSystem.activeMenu = activeMenu;
  }

  public static void setState(MenuState selectedState) {
    activeMenu = selectedState;
  }

  @Override
  public void execute() {
    activeMenu.execute();
  }
}
