package org.example.menuSystem;

import java.util.List;
import org.example.StaffManagementSystem;
import org.example.staff.Employee;
import org.example.staff.Intern;

public class MainMenu extends Menu {
  public MainMenu() {
    super("Main Menu");
    menuOptions =
        List.of(
            new MenuOption(1, "Add An Employee", Employee::addEmployee),
            new MenuOption(2, "Add An Intern", Intern::AddIntern),
            new MenuOption(
                3, "Go To Statistics Menu", () -> MenuSystem.setState(new StatisticsMenu())),
            new MenuOption(4, "Exit", StaffManagementSystem::exit));
  }
}
