package org.example.menuSystem;

public interface MenuState {
  void execute();
}
