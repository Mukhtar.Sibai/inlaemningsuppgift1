package org.example.menuSystem;

import java.util.List;
import java.util.Scanner;

public abstract class Menu implements MenuState {
  protected String name;
  protected List<MenuOption> menuOptions;

  public Menu(String name) {
    this.name = name;
  }

  private void displayMenuOptions() {
    String menuText = name;
    for (MenuOption menuOption : menuOptions) {
      menuText =
          menuText.concat("\n" + menuOption.getOptionNumber() + "-" + menuOption.getOptionText());
    }
    System.out.println(menuText);
    System.out.println("Please enter your choice: ");
  }

  @Override
  public void execute() {
    displayMenuOptions();
    Scanner scanner = new Scanner(System.in);
    int choice = scanner.nextInt();
    for (MenuOption menuOption : menuOptions) {
      if (menuOption.getOptionNumber() == choice) {
        menuOption.run();
      }
    }
  }
}
