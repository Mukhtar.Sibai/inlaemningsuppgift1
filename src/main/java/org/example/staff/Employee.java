package org.example.staff;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Employee extends Staff {

  private final LocalDate startDate;
  private final double salary;

  public Employee(String name, char sex, double salary, LocalDate startDate) {
    super(name, sex);
    this.salary = salary;
    this.startDate = startDate;
  }

  public static void addEmployee() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter employee name ");
    String name = scanner.next();
    System.out.println("Please enter employee sex (M or F)");
    char sex = scanner.next().toUpperCase().charAt(0);
    System.out.println("Please enter employee salary ");
    double salary = scanner.nextDouble();
    System.out.println("Please enter employee start date (yyyy-mm-dd) ");
    LocalDate startDate = LocalDate.parse(scanner.next(), DateTimeFormatter.ISO_LOCAL_DATE);

    Employee employee = new Employee(name, sex, salary, startDate);
    System.out.println(employee.getName() + " has been added successfully.");
  }

  private static List<Employee> getEmployeesList() {

    return Staff.getStaffList().stream()
        .filter(employee -> employee instanceof Employee)
        .map(employee -> (Employee) employee)
        .toList();
  }

  public static void getTheAverageSalaryForMen() {
    List<Employee> employeesList = getEmployeesList();
    double sum = 0;
    int total = 0;
    for (Employee employee : employeesList) {
      if (employee.getSex() == 'M') {
        sum += employee.getSalary();
        total++;
      }
    }
    System.out.println("The Average Salary For Men: " + sum / total);
  }

  public static void getTheAverageSalaryForWomen() {
    List<Employee> employeesList = getEmployeesList();
    double sum = 0;
    int total = 0;
    for (Employee employee : employeesList) {
      if (employee.getSex() == 'F') {
        sum += employee.getSalary();
        total++;
      }
    }
    System.out.println("The Average Salary For Women: " + sum / total);
  }

  public static void arrangeByDate() {
    List<Employee> employeesList =
        Staff.getStaffList().stream()
            .filter(employee -> employee instanceof Employee)
            .map(employee -> (Employee) employee)
            .sorted(Comparator.comparing(Employee::getStartDate))
            .toList();

    for (Employee employee : employeesList) {
      System.out.println(employee);
    }
  }

  public double getSalary() {
    return salary;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  @Override
  public String toString() {
    return "Employee{"
        + super.toString()
        + ", Salary="
        + salary
        + ", Start date="
        + startDate
        + '}';
  }
}
