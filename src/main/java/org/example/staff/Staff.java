package org.example.staff;

import java.util.ArrayList;
import java.util.List;

public abstract class Staff {

  private static final List<Staff> staffList = new ArrayList<>();

  private static int nextId = 1;
  private int id;
  private String name;
  private char sex;

  public Staff(String name, char sex) {
    this.id = nextId++;
    this.name = name;
    this.sex = sex;
    staffList.add(this);
  }

  public static List<Staff> getStaffList() {
    return staffList;
  }

  public static void viewStaffList() {
    if (staffList.isEmpty()) {
      System.out.println("No staff to display.");
    } else {
      System.out.println("Staff List:");
      for (Staff staff : staffList) {
        System.out.println(staff);
      }
    }
  }

  public static void getNumberOfStaff() {
    System.out.println(getStaffList().size());
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public char getSex() {
    return sex;
  }

  public void setSex(char sex) {
    this.sex = sex;
  }

  @Override
  public String toString() {
    return "id=" + id + ", name='" + name + '\'' + ", sex=" + sex;
  }
}
