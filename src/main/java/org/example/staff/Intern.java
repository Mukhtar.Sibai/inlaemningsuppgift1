package org.example.staff;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Intern extends Staff {
  private final LocalDate endDate;
  private final String quitMessage;

  public Intern(String name, char sex, LocalDate endDate, String message) {
    super(name, sex);
    this.endDate = endDate;
    this.quitMessage = message;
  }

  public static void AddIntern() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter intern name ");
    String name = scanner.next();
    System.out.println("Please enter intern sex (M or F)");
    char sex = scanner.next().toUpperCase().charAt(0);
    System.out.println("Please enter a message ");
    String message = scanner.next();
    System.out.println("Please enter intern end date (yyyy-mm-dd) ");
    LocalDate endDate = LocalDate.parse(scanner.next(), DateTimeFormatter.ISO_LOCAL_DATE);

    Intern intern = new Intern(name, sex, endDate, message);
    System.out.println(intern.getName() + " has been added successfully.");
  }

  @Override
  public String toString() {
    return "Intern{"
        + super.toString()
        + ", End date="
        + endDate
        + ", Message='"
        + quitMessage
        + '\''
        + '}';
  }
}
