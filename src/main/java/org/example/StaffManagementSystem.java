package org.example;

import java.time.LocalDate;
import org.example.menuSystem.MainMenu;
import org.example.menuSystem.MenuSystem;
import org.example.staff.Employee;
import org.example.staff.Intern;

public class StaffManagementSystem {
  public static void main(String[] args) {

    new Employee("Max", 'M', 50000, LocalDate.of(2022, 9, 19));
    new Employee("Axel", 'M', 40000, LocalDate.of(2019, 7, 1));
    new Employee("Peter", 'M', 67000, LocalDate.of(2023, 5, 19));
    new Employee("Karl", 'M', 36000, LocalDate.of(2013, 2, 1));
    new Employee("Maria", 'F', 50000, LocalDate.of(2015, 9, 19));
    new Employee("Anna", 'F', 40000, LocalDate.of(2013, 7, 1));

    new Intern("Johanna", 'F', LocalDate.of(2022, 9, 19), "Thank You!");
    new Intern("Caroline", 'F', LocalDate.of(2021, 6, 19), "Thank You!");

    MenuSystem menuSystem = new MenuSystem(new MainMenu());
    while (true) {
      menuSystem.execute();
    }
  }

  public static void exit() {
    System.out.println("Exiting the program.");
    System.exit(0);
  }
}
